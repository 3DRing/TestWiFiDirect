package com.ringov.wifidirect;

import android.content.Context;
import android.net.wifi.WifiManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by Sergey Koltsov on 20.04.2017.
 */

public class Utils {

    private final static String p2pInt = "p2p-p2p0";

    public static String getIPFromMac(String MAC) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4) {
                    // Basic sanity check
                    String device = splitted[5];
                    if (device.matches(".*" +p2pInt+ ".*")){
                        String mac = splitted[3];
                        if (mac.matches(MAC)) {
                            return splitted[0];
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String getLocalIPAddress(Context context) {

        WifiManager wm = (WifiManager) context.getSystemService(WIFI_SERVICE);
        String ipAddress = BigInteger.valueOf(wm.getDhcpInfo().netmask).toString();
        return ipAddress;
    }

    private static String getDottedDecimalIP(byte[] ipAddr) {
        String ipAddrStr = "";
        for (int i=0; i<ipAddr.length; i++) {
            if (i > 0) {
                ipAddrStr += ".";
            }
            ipAddrStr += ipAddr[i]&0xFF;
        }
        return ipAddrStr;
    }
}
