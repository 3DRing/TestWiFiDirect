package com.ringov.wifidirect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WiFiDirectActivity extends AppCompatActivity implements WifiP2pManager.ChannelListener,
        WifiP2pManager.PeerListListener,
        WifiP2pManager.ConnectionInfoListener,
        ServerAsyncTask.ServerStatus,
        ServerAsyncTask.Callback {

    public static final String IP_SERVER = "192.168.49.1";

    public static final String TAG = "wifidirectdemo";
    private static final int PORT = 8888;
    private WifiP2pManager manager;
    private boolean isWifiP2pEnabled = false;
    private boolean retryChannel = false;

    private final IntentFilter intentFilter = new IntentFilter();
    private WifiP2pManager.Channel channel;
    private BroadcastReceiver receiver = null;

    private List<WifiP2pDevice> peers = new ArrayList<>();
    private WifiP2pDevice device;

    private boolean server_running;

    Button btnStart;
    Button btnSend;
    Button btnSearch;
    Button btnConnect;

    /**
     * @param isWifiP2pEnabled the isWifiP2pEnabled to set
     */
    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!server_running) {
                    new ServerAsyncTask(WiFiDirectActivity.this,
                            WiFiDirectActivity.this,
                            WiFiDirectActivity.this, PORT).execute();
                    server_running = true;
                }
            }
        });

        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String localIP = Utils.getLocalIPAddress(WiFiDirectActivity.this);
                String client_mac_fixed = new String(device.deviceAddress).replace("99", "19");
                String clientIP = Utils.getIPFromMac(client_mac_fixed);

                Intent serviceIntent = new Intent(WiFiDirectActivity.this, StringTransferService.class);
                serviceIntent.setAction(StringTransferService.ACTION_SEND_STRING);
                serviceIntent.putExtra(StringTransferService.EXTRAS_STRING, "test string");

                if (localIP.equals(IP_SERVER)) {
                    serviceIntent.putExtra(StringTransferService.EXTRAS_ADDRESS, clientIP);
                } else {
                    serviceIntent.putExtra(StringTransferService.EXTRAS_ADDRESS, IP_SERVER);
                }

                serviceIntent.putExtra(StringTransferService.EXTRAS_PORT, PORT);
                WiFiDirectActivity.this.startService(serviceIntent);
            }
        });

        btnSearch = (Button) findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(WiFiDirectActivity.this, "Discovery Initiated",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(WiFiDirectActivity.this, "Discovery Failed : " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnConnect = (Button) findViewById(R.id.btn_connect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                connect(config);
            }
        });

        // add necessary intent values to be matched.

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
    }

    /**
     * register the BroadcastReceiver with the intent values to be matched
     */
    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void connect(WifiP2pConfig config) {
        manager.connect(channel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(WiFiDirectActivity.this, "Connect failed. Retry.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onChannelDisconnected() {
        // we will try once more
        if (manager != null && !retryChannel) {
            Toast.makeText(this, "Channel lost. Trying again", Toast.LENGTH_LONG).show();
            retryChannel = true;
            manager.initialize(this, getMainLooper(), this);
        } else {
            Toast.makeText(this,
                    "Severe! Channel is probably lost premanently. Try Disable/Re-Enable P2P.",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void cancelDisconnect() {

        /*
         * A cancel abort request by user. Disconnect i.e. removeGroup if
         * already connected. Else, request WifiP2pManager to abort the ongoing
         * request
         */
        if (manager != null) {
            if (device == null
                    || device.status == WifiP2pDevice.CONNECTED) {
                // disconnect
            } else if (device.status == WifiP2pDevice.AVAILABLE
                    || device.status == WifiP2pDevice.INVITED) {

                manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(WiFiDirectActivity.this, "Aborting connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(WiFiDirectActivity.this,
                                "Connect abort request failed. Reason Code: " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        this.peers.clear();
        this.peers.addAll(peers.getDeviceList());
        if (this.peers.size() == 0) {
            Log.d(WiFiDirectActivity.TAG, "No devices found");
            return;
        } else {
            device = this.peers.get(0);
        }
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        // show connection info

    }

    @Override
    public boolean isRunning() {
        return server_running;
    }

    @Override
    public void setRunning(boolean isRunning) {
        server_running = isRunning;
    }

    @Override
    public void call(String string) {
        Toast.makeText(this, "Got it! - " + string, Toast.LENGTH_SHORT).show();
    }
}
