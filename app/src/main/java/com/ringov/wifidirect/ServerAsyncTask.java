package com.ringov.wifidirect;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Sergey Koltsov on 20.04.2017.
 */

/**
 * A simple server socket that accepts connection and writes some data on
 * the stream.
 */
public class ServerAsyncTask extends AsyncTask<Void, Void, String> {

    private final Context context;
    private int port;
    private Callback callback;
    private ServerStatus serverStatus;

    /**
     * @param context
     */
    public ServerAsyncTask(Context context, Callback callback, ServerStatus serverStatus, int port) {
        this.port = port;
        this.context = context;
        this.serverStatus = serverStatus;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            Log.d(WiFiDirectActivity.TAG, "Server: Socket opened");
            Socket client = serverSocket.accept();
            Log.d(WiFiDirectActivity.TAG, "Server: connection done");

            InputStream in = client.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String read;

            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            serverSocket.close();
            serverStatus.setRunning(false);
            return sb.toString();
        } catch (IOException e) {
            Log.e(WiFiDirectActivity.TAG, e.getMessage());
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            callback.call(result);
        }

    }

    /*
     * (non-Javadoc)
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {

    }

    interface Callback {
        void call(String string);
    }

    interface ServerStatus {
        boolean isRunning();

        void setRunning(boolean isRunning);
    }
}